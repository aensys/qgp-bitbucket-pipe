# Bitbucket Pipelines Pipe: QulityGate pipe

This pipe is for validating source with QualityGate.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: aensys/qulitygate-pipe:0.0.1
  variables:
      NAME: "<string>"
      # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable                      | Usage                      |
| ---                           | ---                        |
| QUALITYGATE_ACCESS_TOKEN (*)  | QualityGate API token      |
| QUALITYGATE_PROJECT_NAME (*)  | Name of the QualityGate projects which are related to this Bitbucket project. Example for one project `project_1` in case of multiple projects: `project_1, project_2, project_3, ...`|
| DEBUG                         | Enables logging for debug information. Default: `False` |

_(*) = required variable._

## Prerequisites

## Examples

Basic example:

```yaml
script:
    - pipe: atlassian/demo-pipe-python:0.1.2
      variables:
          NAME: "foobar"
```

Advanced example:

```yaml
script:
    - pipe: atlassian/demo-pipe-python:0.1.2
      variables:
          NAME: "foobar"
          DEBUG: "true"
```

## Support

