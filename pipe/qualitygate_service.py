import backoff
from bitbucket_pipes_toolkit import Pipe
from colorlog import colorlog

from qualitygate_api import QualityGateApi, MAX_BACKOFF_TIMEOUT


class QualityGateService:

    def __init__(self, pipe):
        self._pipe: Pipe = pipe
        self._pipe.logger.handlers.__getitem__(0).setFormatter(colorlog.ColoredFormatter(
            '%(log_color)s%(asctime)s %(levelname)-6s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
        self._quality_gate_api = QualityGateApi(pipe)

    def get_quality_result(self, qg_project_name, branch_name, commit_hash):
        qg_project_id = self._find_project_id_by_name(qg_project_name)

        branch_id = self._find_branch_id(qg_project_name, qg_project_id, branch_name)
        version_id = self._find_version_id(qg_project_name, branch_id, commit_hash)
        return self._find_quality_profile(qg_project_name, version_id)

    def _find_project_id_by_name(self, project_name):

        self._pipe.log_info(
            f"[{project_name}] Searching project id by name")

        qg_projects = self._quality_gate_api.get_projects()

        if not qg_projects:
            self._pipe.log_warning(
                f"[{project_name}] No QualityGate project is found for the given token")

        for qg_project in qg_projects:
            current_project_name = qg_project.get('projectName')

            self._pipe.log_debug(
                f"[{project_name}] Current project name for checking: '{current_project_name}'")

            if current_project_name == project_name:
                return qg_project.get('id')

        raise ValueError(f"[{project_name}] Project id NOT found for project name")

    @backoff.on_exception(backoff.fibo, ValueError, max_value=100, max_time=MAX_BACKOFF_TIMEOUT)
    def _find_version_id(self, qg_project_name, branch_id, commit_hash):
        self._pipe.log_info(
            f"[{qg_project_name}] Searching version for branch id: '{branch_id}' and commit: '{commit_hash}'")

        versions = self._quality_gate_api.get_version(branch_id)

        for version in versions:
            if version.get('hash') == commit_hash:
                return version.get('id')

        raise ValueError(
            f"[{qg_project_name}] Version not found for branch id: '{branch_id}' and hash: '{commit_hash}'")

    def _find_branch_id(self, qg_project_name, qg_project_id, branch_name):
        self._pipe.log_info(
            f"[{qg_project_name}] Searching branch id for project: '{qg_project_id}' and branch name: '{branch_name}'")

        branches = self._quality_gate_api.get_branches(qg_project_id)

        for branch in branches:
            current_branch_name = branch.get('branchName')
            self._pipe.log_debug(
                f"[{qg_project_name}] Current branch for checking: '{current_branch_name}'")
            if current_branch_name == branch_name:
                return branch.get('id')

        raise ValueError(f"[{qg_project_name}] Branch not found: '{branch_name}'")

    def _find_quality_profile(self, qg_project_name, version_id):
        self._pipe.log_info(
            f"[{qg_project_name}] Searching quality profile for branch version: '{version_id}'")

        quality = self._quality_gate_api.get_quality_profile(version_id)

        if quality is None:
            raise ValueError(
                f"[{qg_project_name}] No quality profile found for version: '{version_id}'")

        return quality
